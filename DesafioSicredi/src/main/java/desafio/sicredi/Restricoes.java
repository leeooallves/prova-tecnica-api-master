package desafio.sicredi;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;

public class Restricoes {
    @BeforeAll
    public static void setup() {
        baseURI = "http://localhost";
        port = 8080;
    }

    @Test
    public void ConsultaCpfRestricao1() {
        given().
                log().all().
                pathParam("cpf", "60094146012").
                when().
                    get("/api/v1/restricoes/{cpf}").
                then().
                    statusCode(200).
                    body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao2() {
        given().
                log().all().
                pathParam("cpf", "60094146012").
                when().
                    get("/api/v1/restricoes/{cpf}").
                then().
                    statusCode(200).
                    body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao3() {
        given().
                log().all().
                pathParam("cpf", "84809766080").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao4() {
        given().
                log().all().
                pathParam("cpf", "26276298085").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao5() {
        given().
                log().all().
                pathParam("cpf", "26276298085").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao6() {
        given().
                log().all().
                pathParam("cpf", "01317496094").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao7() {
        given().
                log().all().
                pathParam("cpf", "55856777050").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao8() {
        given().
                log().all().
                pathParam("cpf", "19626829001").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao9() {
        given().
                log().all().
                pathParam("cpf", "24094592008").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfRestricao10() {
        given().
                log().all().
                pathParam("cpf", "58063164083").
                when().
                get("/api/v1/restricoes/{cpf}").
                then().
                statusCode(200).
                body("mensagem", Matchers.containsString("tem problema"))
        ;
    }

    @Test
    public void ConsultaCpfSemRestricao() {
        //CPFs sem restrições gerado para teste
        given().
                log().all().
                pathParam("cpf", "92279432005").
                when().
                    get("/api/v1/restricoes/{cpf}").
                then().
                    statusCode(204)
        ;
    }

}
