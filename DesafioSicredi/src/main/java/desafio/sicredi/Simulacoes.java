package desafio.sicredi;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;

public class Simulacoes {
    @BeforeAll
    public static void setup(){
        baseURI = "http://localhost";
        port = 8080;
    }

    @Test
    public void CriarSimulacao(){
        given().
                log().all().
                contentType("application/json").
                body("{\"nome\": \"Noberto\",\"cpf\": \"69858475012\",\"email\": \"roberto@gmail.com\",\"valor\": 15500.00, \"parcelas\": 5,\"seguro\": true}").
        when().
                post("/api/v1/simulacoes").
        then().
                statusCode(201).
                body("nome",Matchers.is("Noberto")).
                body("id",Matchers.is(Matchers.notNullValue()))
        ;
    }

    @Test
    public void CriarSimulacaoDuplicado(){
        given().
                log().all().
                contentType("application/json").
                body("{\"nome\": \"teste\",\"cpf\": \"66414919004\",\"email\": \"email@email.com\",\"valor\": 15500.00, \"parcelas\": 5,\"seguro\": true}").
        when().
                post("/api/v1/simulacoes").
        then().
                statusCode(400).
                body("mensagem", Matchers.is("CPF duplicado"));

        ;
    }

    @Test
    public void AlterarSimulacao(){
        given().
                log().all().
                contentType("application/json").
                pathParam("cpf","69858475012").
                body("{\n" +
                        "  \"nome\": \"teste\",\n" +
                        "  \"cpf\": 69858475012,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 1200,\n" +
                        "  \"parcelas\": 3,\n" +
                        "  \"seguro\": true\n" +
                        "}").
        when().
                put("/api/v1/simulacoes/{cpf}").
        then().
                statusCode(200).
                body("id", Matchers.is(Matchers.notNullValue())).
                body("nome",Matchers.is("teste"))
        ;
    }

    @Test
    public void ConsultarSimulacoes(){
        given().
                log().all().
        when().
                get("/api/v1/simulacoes").
        then().
                statusCode(200).
                body("nome",Matchers.hasItem("Deltrano"))
        ;
    }

    @Test
    public void ConsultarSimulacaoNaoIncluido(){
        given().
                log().all().
                pathParam("cpf","02324814072").
        when().
                get("/api/v1/simulacoes/{cpf}").
        then().
                statusCode(404)
        ;
    }

    @Test
    public void ConsultaSimulacaoCPF(){
        given().
                log().all().
                pathParam("cpf", "17822386034").
        when().
                get("/api/v1/simulacoes/{cpf}").
        then().
                statusCode(200).
                body("id",Matchers.is(12)).
                body("nome", Matchers.is("Deltrano"))

        ;
    }

    @Test
    public void RemoverSimulacao(){
        given().
                log().all().
                pathParam("id", "12").
        when().
                delete("/api/v1/simulacoes/{id}").
        then().
                statusCode(204).
                body(Matchers.is("OK"))
        ;
    }

    @Test
    //Nesse teste a API está retornando Código 200 mesmo que a Id não tenha registro!
    public void RemoverSimulacaoNaoEncontrada(){
        given().
                log().all().
                pathParam("id",55).
        when().
                delete("/api/v1/simulacoes/{id}").
        then().
                statusCode(200)
                //statusCode(404)
        ;
    }

}
